package com.young.mall.config.security.service;

import org.springframework.core.Ordered;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/6/4 16:49
 */
public interface YoungMallUserDetailsService extends UserDetailsService, Ordered {


    /**
     * 是否支持此客户端校验
     *
     * @param grantType
     * @return
     */
    default boolean support(String grantType) {
        return true;
    }

    /**
     * 排序值 默认取最大的
     *
     * @return 排序值
     */
    default int getOrder() {
        return 0;
    }

}
