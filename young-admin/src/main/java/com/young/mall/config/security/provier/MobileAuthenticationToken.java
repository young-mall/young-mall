package com.young.mall.config.security.provier;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/6/4 18:31
 */
public class MobileAuthenticationToken extends AbstractAuthenticationToken {

    private final Object principal;
    private final Object credentials;


    private final String grantType;

    /**
     * 验证码/密码
     */
    private String code;

    public MobileAuthenticationToken(Object principal, Object credentials, String grantType) {
        super(null);
        this.principal = principal;
        this.credentials = credentials;
        this.grantType = grantType;
        setAuthenticated(false);
    }

    public MobileAuthenticationToken(Object principal, Object credentials, String code, String grantType) {
        super(AuthorityUtils.NO_AUTHORITIES);
        this.principal = principal;
        this.credentials = credentials;
        this.code = code;
        this.grantType = grantType;
    }

    @Override
    public Object getCredentials() {
        return this.principal;
    }

    @Override
    public Object getPrincipal() {
        return this.code;
    }
}
