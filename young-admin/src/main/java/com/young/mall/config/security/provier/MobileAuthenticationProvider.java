package com.young.mall.config.security.provier;

import cn.hutool.extra.spring.SpringUtil;
import com.young.mall.config.security.service.YoungMallUserDetailsService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;

import java.util.Map;

/**
 * @author yqz
 * @Description
 * @CreateDate 2022/6/4 16:44
 */
@Slf4j
public class MobileAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {


    @Setter
    private UserDetailsChecker preAuthenticationChecks = new AccountStatusUserDetailsChecker();

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {
        if (usernamePasswordAuthenticationToken.getCredentials() == null) {
            this.logger.debug("Failed to authenticate since no credentials provided");
            throw new BadCredentialsException(this.messages
                    .getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
        }
    }

    @Override
    protected UserDetails retrieveUser(String s, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {

        return null;
    }


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        if (authentication.getCredentials() == null) {
            log.debug("Failed to authenticate since no credentials provided");
            throw new BadCredentialsException("Bad credentials");
        }

        MobileAuthenticationToken requestToken = (MobileAuthenticationToken) authentication;

        Map<String, YoungMallUserDetailsService> userDetailsServiceMap = SpringUtil.getBeansOfType(YoungMallUserDetailsService.class);


        return super.authenticate(authentication);
    }


    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.isAssignableFrom(MobileAuthenticationToken.class);
    }
}
